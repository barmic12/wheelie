﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour {
    public float thrust;
    public Rigidbody rb;
    public Camera fpsCam;
    public float force = 1000f;
    public float range = 100f;
    private float distBetweenFpsAndTire;
    public GameObject tire;
    private bool IsPicked;
    private Quaternion TireRotation;
    private Vector3 TireEulerRotation;
    float PlayerForce = 1000;
    float PlayerForceAccelerationPerSecond = 1000f;
    float MaxPlayerForce = 10000;
    //public Camera cam = GameObject.Find("First Person Controller/Main Camera").GetComponent(Camera);
    public Camera camFps;
        // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        camFps = Camera.main;
        tire = GameObject.Find("Tire");
        IsPicked = false;
        TireRotation = tire.transform.rotation;
        TireEulerRotation = tire.transform.eulerAngles;
    }
	
	// Update is called once per frame
	void Update () {
        distBetweenFpsAndTire = Vector3.Distance(rb.position, camFps.transform.position);
        //print("Distance to other: " + distBetweenFpsAndTire);
        //print("Your coords are: " + camFps.transform.position.x + " " + camFps.transform.position.y + " " + camFps.transform.position.z);
        //print("Rotation of tire: " + tire.transform.rotation);
        //        print("Rotation of character: " + camFps.transform.rotation);
        //print("Camera y rotation: " + camFps.transform.rotation.y);
        print("Current player force: " + PlayerForce.ToString());
        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.JoystickButton2))
        {
            if (IsPicked)
            {
                Vector3 newPosition = new Vector3(camFps.transform.position.x + 3, camFps.transform.position.y, camFps.transform.position.z);
                //TireRotation.z = camFps.transform.rotation.z;
                TireRotation.y = camFps.transform.localRotation.y;
                TireEulerRotation.y = camFps.transform.eulerAngles.y;
                //TireRotation.z = camFps.transform.rotation.z;
                Vector3 newP = new Vector3();
                newP = tire.transform.eulerAngles;
                //tire.transform.rotation = TireRotation;
                tire.transform.eulerAngles = TireEulerRotation;
                //tire.transform.rotation.x = camFps.transform.rotation.w;
                //tire.transform.localPosition = newPosition;
                print("Rotation of camera: " + camFps.transform.rotation );
                tire.transform.localPosition = camFps.transform.position + camFps.transform.forward * 2;
                FindObjectOfType<AudioManager>().Play("Placing");
                IsPicked = false;
            }
            
            //Funny feature
            //Vector3 newPosition = new Vector3(camFps.transform.position.x, camFps.transform.position.y, camFps.transform.position.z + 5);
            //transform.position = newPosition;
            //Instantiate(rb, newPosition, rb.rotation);

            
        }

        if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.JoystickButton2))
        {
            if (!IsPicked && distBetweenFpsAndTire < 3.7)
            {
                Vector3 newPosition = new Vector3(1000, 2.531f, 400f);
                Vector3 newP = new Vector3();
                newP = tire.transform.position;
                newP.x = 1000;
                //rb.isKinematic = false;
                tire.transform.position = newP;
                //tire.transform.position = newPosition;
                FindObjectOfType<AudioManager>().Play("Grab");
                IsPicked = true;
            }
        }

        if (Input.GetKey(KeyCode.F) || Input.GetKey(KeyCode.JoystickButton1))
        {
            
            if (PlayerForce < MaxPlayerForce)
            {
                PlayerForce += PlayerForceAccelerationPerSecond * Time.deltaTime;
            }
            else
            {
                PlayerForce = MaxPlayerForce;
            }
        }
        else
        {

            if (PlayerForce > 1000)
            {
                PlayerForce -= PlayerForceAccelerationPerSecond * Time.deltaTime; //possibly speedDeaccelerationPerSecond depending on your needs
            }
            else
            {
                PlayerForce = 1000;
            }
        }

        if (Input.GetKeyUp(KeyCode.F) || Input.GetKeyUp(KeyCode.JoystickButton1)) {
            if (distBetweenFpsAndTire < 3.7)
            {
                rb.AddForce(camFps.transform.forward * PlayerForce);
                FindObjectOfType<AudioManager>().Play("Kicking");
            }
            else
            {
                Debug.Log("You are too far to kick tire!");
            }
        }
    }

    //private void OnMouseDown()
    //{
    //    Debug.Log("Camera position: " + camFps.transform.position);
        
        //Vector3 direction = rb.transform.position - transform.position;
        //rb.AddForceAtPosition(direction.normalized * 500, transform.position);
        //rb.AddForce(-transform.forward * 5000);
    //}
}
