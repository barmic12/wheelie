﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSound : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        float impactThreshold = 0.1f;

        if (collision.relativeVelocity.magnitude > impactThreshold)
        {
            if (collision.gameObject.name == "Tire")
            {
                Debug.Log("Bounce: " + collision.relativeVelocity.magnitude.ToString());
                FindObjectOfType<AudioManager>().Play("Box");
            }

        }
    }
}
