﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{

    public GameObject cameraOne;
    public GameObject cameraTwo;
    public GameObject cameraThree;

    AudioListener cameraOneAudioLis;
    AudioListener cameraTwoAudioLis;
    AudioListener cameraThreeAudioLis;

    // Use this for initialization
    void Start()
    {

        //Get Camera Listeners
        cameraOneAudioLis = cameraOne.GetComponentInChildren<AudioListener>();
        cameraTwoAudioLis = cameraTwo.GetComponent<AudioListener>();
        cameraThreeAudioLis = cameraThree.GetComponent<AudioListener>();

        PlayerPrefs.SetInt("CameraPosition", 0);
        //Camera Position Set
        cameraPositionChange(PlayerPrefs.GetInt("CameraPosition"));

        cameraOneAudioLis.enabled = false;
        cameraOne.SetActive(false);
        cameraTwoAudioLis.enabled = false;
        cameraTwo.SetActive(false);
        cameraThreeAudioLis.enabled = false;
        cameraThree.SetActive(false);
        cameraOneAudioLis.enabled = true;
        cameraOne.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        //Change Camera Keyboard
        switchCamera();
    }

    //UI JoyStick Method
    public void cameraPositonM()
    {
        cameraChangeCounter();
    }

    //Change Camera Keyboard
    void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.C) || Input.GetKeyDown(KeyCode.JoystickButton4))
        {
            cameraChangeCounter();
        }
    }

    //Camera Counter
    void cameraChangeCounter()
    {
        int cameraPositionCounter = PlayerPrefs.GetInt("CameraPosition");
        cameraPositionCounter++;
        cameraPositionChange(cameraPositionCounter);
    }

    //Camera change Logic
    void cameraPositionChange(int counter)
    {
        Debug.Log("Counter: " + counter.ToString());
        if (counter > 2)
        {
            counter = 0;
        }
        Debug.Log("Counter: " + counter.ToString());
        //Set camera position database
        PlayerPrefs.SetInt("CameraPosition", counter);

        //Set camera position 1
        if (counter == 0)
        {
            cameraOne.SetActive(true);
            cameraOneAudioLis.enabled = true;

            Debug.Log("Camera One Active");

            cameraThreeAudioLis.enabled = false;
            cameraThree.SetActive(false);

        }

        //Set camera position 2
        if (counter == 1)
        {
            cameraTwo.SetActive(true);
            cameraTwoAudioLis.enabled = true;

            Debug.Log("Camera Two Active");

            cameraOneAudioLis.enabled = false;
            cameraOne.SetActive(false);
        }

        //Set camera position 3
        if (counter == 2)
        {
            cameraThree.SetActive(true);
            cameraThreeAudioLis.enabled = true;

            Debug.Log("Camera Three Active");

            cameraTwoAudioLis.enabled = false;
            cameraTwo.SetActive(false);
        }

    }
}