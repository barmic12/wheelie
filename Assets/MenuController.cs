﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public GameObject PauseScreen;
	public bool isPaused;

	
	// Use this for initialization
	void Start () {
		isPaused = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.JoystickButton7))
		{
			if(isPaused)
			{
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                isPaused = false;
				PauseScreen.SetActive(false);
				Time.timeScale = 1f;
			}
			else{
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
                isPaused = true;
				PauseScreen.SetActive(true);
				Time.timeScale = 0f;
			}
		}
	}

    public void QuitGame()
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("WheelieScene");
    }
}
