﻿using System.Collections.Generic;
using UnityEngine;
public class DustController : MonoBehaviour {
    public GameObject object_;
    [Range(0f, 0.2f)]
    public float particlesMultiplier = 0.2f;
    public List<GameObject> dustPlanes = new List<GameObject>();

    Vector3 oldpos;
    //tire have y center in 1f, try to fetch at start
    float tireYoffset = 1f;
	
	void Start () 
    {
        Debug.Assert(object_ != null);
        oldpos = object_.transform.position;
        tireYoffset = oldpos.y;
	}

    bool isInsideOfAnyOfDustPlanes()
    {
        var objectPosition = object_.GetComponent<Collider>().bounds;
        foreach (var dustPlane in dustPlanes)
        {
            if(dustPlane.GetComponent<Collider>().bounds.Intersects(objectPosition))
            {
                return true;
            }
        }

        return false;
    }
	
	void FixedUpdate () 
    {
        if (!isInsideOfAnyOfDustPlanes())
        {
            return;
        }

        var particles = GetComponent<ParticleSystem>();
        var shapeModule = particles.shape;
        shapeModule.position = new Vector3(object_.transform.position.x,
                                           tireYoffset - object_.transform.position.y - object_.transform.localScale.y / 2f, 
                                           object_.transform.position.z);
        var newpos = object_.transform.position;
        var media = (newpos - oldpos);
        var velocity = media / Time.deltaTime;
        oldpos = newpos;

        if (velocity != Vector3.zero)
        {
            var velocityMgn = velocity.sqrMagnitude;
            var particlesf =  velocityMgn * (particlesMultiplier);
            var particlesCount = Mathf.RoundToInt(particlesf);
            GetComponent<ParticleSystem>().Emit(particlesCount);
        }
	}
}
