﻿using UnityEngine;

public class WallController : MonoBehaviour {
    [Range(1,20)]
    public uint rows = 10;
    [Range(1, 20)]
    public uint width = 20;
    [Range(0f, 2f)]
    public float buildingBlockMass = 0.5f;
    [Range(0f, 360f)]
    public uint rotation = 0;

    public GameObject buildingBlockPrefab;
    float buildingBlockWidth_ = 1f;
    const float xOffsetToAvoidRbExplosion = 0.1f;
    const float yOffsetToAvoidRbExplosion = 0.2f;

	void Start () {
        Debug.Assert(buildingBlockPrefab, "Failed to load building block prefab");
        buildingBlockWidth_ = buildingBlockPrefab.transform.localScale.x / 2f; 

        for (uint x = 0; x < width; ++x)
        {
            for (uint y = 0; y < rows; ++y)
            {
                var place = getPlace(x, y);
                var brick_ = Instantiate(buildingBlockPrefab, place, Quaternion.identity, transform);
                brick_.GetComponent<Rigidbody>().mass = buildingBlockMass;
            }
        }
        transform.rotation = Quaternion.Euler(0f, rotation, 0f); 
	}

    Vector3 getPlace(uint x, uint y)
    {
        var scale = buildingBlockPrefab.transform;
        var res = new Vector3(transform.position.x + scale.localScale.x * x + xOffsetToAvoidRbExplosion,
                              transform.position.y + scale.localScale.y * y + yOffsetToAvoidRbExplosion, 
                              transform.position.z);
        if(y%2 == 0){
            res.x += buildingBlockWidth_;
        }
        return res;
    }

}
